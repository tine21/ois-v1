import java.util.Scanner;

class Gravitacija {

    //student B
    public static double izracun(double nadmorskaVisina){ 
        
        double c = 6.674 * Math.pow(10,-11); //gravitacijska konstanta
        double m = 5.972 * Math.pow(10,24);  //masa zemlje
        double r = 6.371 * Math.pow(10,6);   //polmer zemlje
        
        double g = c*m/((r+nadmorskaVisina)*(r+nadmorskaVisina));//gravitacijski pospešek
        return g;

    }
    static void izpis(double visina, double pospesek){
        System.out.println("Nadmorka visina: " + visina + " in gravitacijski pospesek: " + pospesek+".");
        int a=0;
        int b=0;
        int c=0;
        int d=0;
        
    }
    
    
	public static void main(String[] args){
	    
	    Scanner sc = new Scanner(System.in);
		
		double v = sc.nextInt(); // nadmorska visina;
		double g = izracun(v);
		
		izpis(v, g);
	    	
		
	}

}
